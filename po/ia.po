# Interlingua translation for lomiri-indicator-network
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lomiri-indicator-network package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-network\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 19:46+0000\n"
"PO-Revision-Date: 2023-03-02 15:36+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Interlingua <https://hosted.weblate.org/projects/lomiri/"
"lomiri-indicator-network/ia/>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16.2-dev\n"
"X-Launchpad-Export-Date: 2016-10-21 07:05+0000\n"

#: doc/qt/qml/examples/example_networking_status.qml:46
msgid "Networking Status"
msgstr ""

#: src/agent/SecretRequest.cpp:61
#, qt-format
msgid "Connect to “%1”"
msgstr "Associar al “%1”"

#: src/agent/SecretRequest.cpp:66
msgid "WPA"
msgstr "WPA"

#: src/agent/SecretRequest.cpp:68
msgid "WEP"
msgstr "WEP"

#: src/agent/SecretRequest.cpp:74
msgid "Connect"
msgstr "Connecter"

#: src/agent/SecretRequest.cpp:75 src/vpn-editor/DialogFile/DialogFile.qml:156
msgid "Cancel"
msgstr "Cancellar"

#: src/indicator/factory.cpp:186
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/indicator/factory.cpp:197
msgid "Flight Mode"
msgstr "Modo volo"

#: src/indicator/factory.cpp:207
msgid "Cellular data"
msgstr "Datos del cellular"

#: src/indicator/factory.cpp:220
msgid "Hotspot"
msgstr "Hotspot"

#: src/indicator/menuitems/wifi-link-item.cpp:109
msgid "Other network…"
msgstr "Altere rete…"

#: src/indicator/menuitems/wwan-link-item.cpp:90
msgid "No SIM"
msgstr "Nulle SIM"

#: src/indicator/menuitems/wwan-link-item.cpp:97
msgid "SIM Error"
msgstr "Error del SIM"

#: src/indicator/menuitems/wwan-link-item.cpp:105
msgid "SIM Locked"
msgstr "SIM Blocate"

#: src/indicator/menuitems/wwan-link-item.cpp:118
msgid "Unregistered"
msgstr "Non registrate"

#: src/indicator/menuitems/wwan-link-item.cpp:123
msgid "Unknown"
msgstr "Incognite"

#: src/indicator/menuitems/wwan-link-item.cpp:128
msgid "Denied"
msgstr "Negate"

#: src/indicator/menuitems/wwan-link-item.cpp:133
msgid "Searching"
msgstr "In recerca"

#: src/indicator/menuitems/wwan-link-item.cpp:145
msgid "No Signal"
msgstr "Nulle signal"

#: src/indicator/menuitems/wwan-link-item.cpp:157
#: src/indicator/menuitems/wwan-link-item.cpp:164
msgid "Offline"
msgstr "Sin connexion"

#: src/indicator/nmofono/vpn/vpn-manager.cpp:87
#, qt-format
msgid "VPN connection %1"
msgstr "Connection VPN %1"

#. TRANSLATORS: this is the indicator title shown on the top header of the indicator area
#: src/indicator/root-state.cpp:314
msgid "Network"
msgstr "Rete"

#: src/indicator/sections/vpn-section.cpp:147
msgid "VPN settings…"
msgstr "Configurationes VPN..."

#: src/indicator/sections/wifi-section.cpp:58
msgid "Wi-Fi settings…"
msgstr "Wi-Fi, preparationes…"

#: src/indicator/sections/wwan-section.cpp:102
msgid "Cellular settings…"
msgstr "Preparationes del cellular…"

#: src/indicator/sim-unlock-dialog.cpp:144
msgid "Sorry, incorrect %{1} PIN."
msgstr "Displacente, incorrecte %{1} PIN."

#: src/indicator/sim-unlock-dialog.cpp:149
#: src/indicator/sim-unlock-dialog.cpp:178
msgid "This will be your last attempt."
msgstr "Isto essera tu ultime tentativa."

#: src/indicator/sim-unlock-dialog.cpp:151
msgid ""
"If %{1} PIN is entered incorrectly you will require your PUK code to unlock."
msgstr ""
"If %{1} PIN es inserite incorrectemente tu requirera tu codice PUK pro "
"disblocar."

#: src/indicator/sim-unlock-dialog.cpp:161
msgid "Sorry, your %{1} is now blocked."
msgstr "Displacente, tu %{1} es ora blocate."

#: src/indicator/sim-unlock-dialog.cpp:166
msgid "Please enter your PUK code to unblock SIM card."
msgstr "Per favor entra tu codice  PUK pro disblocar le carta SIM."

#: src/indicator/sim-unlock-dialog.cpp:168
msgid "You may need to contact your network provider for PUK code."
msgstr "Tu pote deber continger tu suppletor de rete pro le codice PUK."

#: src/indicator/sim-unlock-dialog.cpp:176
#: src/indicator/sim-unlock-dialog.cpp:190
msgid "Sorry, incorrect PUK."
msgstr "Displacente, PUK incorrecte."

#: src/indicator/sim-unlock-dialog.cpp:180
msgid ""
"If PUK code is entered incorrectly, your SIM card will be blocked and needs "
"replacement."
msgstr ""
"Si le codice PUK es inserite incorrectemente, tu carta SIM essera blocate e "
"necessitara le reimplaciamento."

#: src/indicator/sim-unlock-dialog.cpp:182
msgid "Please contact your network provider."
msgstr "Per favor continge le suppletor de rete."

#: src/indicator/sim-unlock-dialog.cpp:192
msgid "Your SIM card is now permanently blocked and needs replacement."
msgstr "Tu carta SIM es ora blocate permanentemente e  necessita reimplaciar."

#: src/indicator/sim-unlock-dialog.cpp:194
msgid "Please contact your service provider."
msgstr "Per favor continge le suppletor de servicio."

#: src/indicator/sim-unlock-dialog.cpp:218
msgid "Sorry, incorrect PIN"
msgstr "Displacente, PIN incorrecte"

#: src/indicator/sim-unlock-dialog.cpp:230
msgid "Sorry, incorrect PUK"
msgstr "Displacente, PUK incorrecte"

#: src/indicator/sim-unlock-dialog.cpp:292
msgid "Enter %{1} PIN"
msgstr "Insere le %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:300
msgid "Enter PUK code"
msgstr "Insere le codice PUK"

#: src/indicator/sim-unlock-dialog.cpp:304
msgid "Enter PUK code for %{1}"
msgstr "Insere  le codice PUK pro %{1}"

#: src/indicator/sim-unlock-dialog.cpp:315
#, c-format
msgid "1 attempt remaining"
msgid_plural "%d attempts remaining"
msgstr[0] "1 tentativa restante"
msgstr[1] "%d tentativas restante"

#: src/indicator/sim-unlock-dialog.cpp:326
msgid "Enter new %{1} PIN"
msgstr "Insere le nove %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:334
msgid "Confirm new %{1} PIN"
msgstr "Confirma le nove %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:373
msgid "PIN codes did not match."
msgstr "Le codice PIN non concorda."

#: src/indicator/vpn-status-notifier.cpp:48
#, qt-format
msgid "The VPN connection '%1' failed."
msgstr "Le connection VPN '%1' ha fallite."

#: src/indicator/vpn-status-notifier.cpp:50
#, qt-format
msgid ""
"The VPN connection '%1' failed because the network connection was "
"interrupted."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le connexion de rete ha essite "
"interrupte."

#: src/indicator/vpn-status-notifier.cpp:51
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service stopped unexpectedly."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le servicio VPN se arrestava "
"inspectatemente"

#: src/indicator/vpn-status-notifier.cpp:52
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service returned invalid "
"configuration."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le servicio VPN retornava "
"configuration non valide."

#: src/indicator/vpn-status-notifier.cpp:53
#, qt-format
msgid ""
"The VPN connection '%1' failed because the connection attempt timed out."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le tentativa de connexion iva foras "
"tempore."

#: src/indicator/vpn-status-notifier.cpp:54
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service did not start in time."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le servicio VPN non ha initiate per "
"tempore."

#: src/indicator/vpn-status-notifier.cpp:55
#, qt-format
msgid "The VPN connection '%1' failed because the VPN service failed to start."
msgstr ""
"Le connexion VPN '%1' ha fallite perque le servicio VPN non ha initiate."

#: src/indicator/vpn-status-notifier.cpp:56
#, qt-format
msgid "The VPN connection '%1' failed because there were no valid VPN secrets."
msgstr ""
"Le connexion VPN '%1' ha fallite perque il non habeva secretos de VPN valide"

#: src/indicator/vpn-status-notifier.cpp:57
#, qt-format
msgid "The VPN connection '%1' failed because of invalid VPN secrets."
msgstr "Le connexion VPN '%1' ha fallite per le secretos de VPN valide"

#: src/indicator/vpn-status-notifier.cpp:68
msgid "VPN Connection Failed"
msgstr "Le connexion VPN ha fallite"

#: src/vpn-editor/DialogFile/DialogFile.qml:164
msgid "Accept"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:113 src/vpn-editor/Pptp/Advanced.qml:24
#: src/vpn-editor/Pptp/Editor.qml:92
msgid "Advanced"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:44
#: src/vpn-editor/Pptp/Advanced.qml:44
msgid "Only use connection for VPN resources"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:48
msgid "Use custom gateway port:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:64
msgid "Use renegotiation interval:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:85
msgid "Use LZO data compression"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:94
#, fuzzy
#| msgid "VPN connection %1"
msgid "Use a TCP connection"
msgstr "Connection VPN %1"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:98
msgid "Use custom virtual device type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:105
msgid "TUN"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:106
msgid "TAP"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:117
msgid "(automatic)"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:121
msgid "and name:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:125
msgid "Use custom tunnel MTU:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:142
msgid "Use custom UDP fragment size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:164
msgid "Restrict tunnel TCP MSS"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:173
msgid "Randomize remote hosts"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:28
#: src/vpn-editor/Openvpn/Editor.qml:132
msgid "Proxies"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:43
msgid "Proxy type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:45
msgid "Not required"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:46
msgid "HTTP"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:47
msgid "SOCKS"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:54
msgid "Server address:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:65
msgid "Port:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:77
msgid "Retry indefinitely:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:87
msgid "Proxy username:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:98
msgid "Proxy password:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:119 src/vpn-editor/Pptp/Advanced.qml:97
msgid "Security"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:39
msgid "Cipher:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:41
#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:83
msgid "Default"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:42
msgid "DES-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:43
msgid "RC2-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:44
msgid "DES-EDE-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:45
msgid "DES-EDE3-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:46
msgid "DESX-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:47
msgid "RC2-40-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:48
msgid "CAST5-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:49
msgid "AES-128-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:50
msgid "AES-192-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:51
msgid "AES-256-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:52
msgid "CAMELLIA-128-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:53
msgid "CAMELLIA-192-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:54
msgid "CAMELLIA-256-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:55
msgid "SEED-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:56
msgid "AES-128-CBC-HMAC-SHA1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:57
msgid "AES-256-CBC-HMAC-SHA1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:64
msgid "Use cipher key size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:81
msgid "HMAC authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:84
#: src/vpn-editor/Openvpn/AdvancedTls.qml:86
#: src/vpn-editor/Openvpn/StaticKey.qml:37
msgid "None"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:85
msgid "RSA MD-4"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:86
msgid "MD-5"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:87
msgid "SHA-1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:88
msgid "SHA-224"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:89
msgid "SHA-256"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:90
msgid "SHA-384"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:91
msgid "SHA-512"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:92
msgid "RIPEMD-160"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:25
msgid "TLS authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:46
msgid "Subject match:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:50
msgid "Verify peer certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:57
msgid "Peer certificate TLS type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:59
msgid "Server"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:60
msgid "Client"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:68
msgid "Use additional TLS authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:75
msgid "Key file:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:84
#: src/vpn-editor/Openvpn/StaticKey.qml:35
msgid "Key direction:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:87
#: src/vpn-editor/Openvpn/StaticKey.qml:38
msgid "0"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:88
#: src/vpn-editor/Openvpn/StaticKey.qml:39
msgid "1"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:65 src/vpn-editor/Pptp/Editor.qml:35
msgid "General"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:74 src/vpn-editor/Pptp/Editor.qml:44
msgid "ID:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:77
msgid "Authentication"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:86
msgid "Remote:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:91
msgid "Certificates (TLS)"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:92
msgid "Password"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:93
msgid "Password with certificates (TLS)"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:94
msgid "Static key"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:98
msgid "Type:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:125
msgid "TLS"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:32
#: src/vpn-editor/Openvpn/PasswordTls.qml:32
msgid "Username:"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:42
#: src/vpn-editor/Openvpn/PasswordTls.qml:42 src/vpn-editor/Pptp/Editor.qml:76
msgid "Password:"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:51
#: src/vpn-editor/Openvpn/PasswordTls.qml:60 src/vpn-editor/Openvpn/Tls.qml:40
msgid "CA certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:51 src/vpn-editor/Openvpn/Tls.qml:31
msgid "User certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:69 src/vpn-editor/Openvpn/Tls.qml:49
msgid "Private key:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:79 src/vpn-editor/Openvpn/Tls.qml:58
msgid "Key password:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:31
msgid "Static key:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:52
msgid "Remote IP:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:62
msgid "Local IP:"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:47
msgid "Authentication methods"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:55
msgid "PAP"
msgstr "PAP"

#: src/vpn-editor/Pptp/Advanced.qml:65
msgid "CHAP"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:75
msgid "MSCHAP"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:84
msgid "MSCHAPv2"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:93
msgid "EAP"
msgstr "EAP"

#: src/vpn-editor/Pptp/Advanced.qml:105
msgid "Use Point-to-Point encryption"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:110
msgid "All available (default)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:111
msgid "128-bit (most secure)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:112
msgid "40-bit (less secure)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:125
msgid "Allow stateful encryption"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:128
msgid "Compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:136
msgid "Allow BSD data compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:145
msgid "Allow Deflate data compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:154
msgid "Use TCP header compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:157
msgid "Echo"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:165
msgid "Send PPP echo packets"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:54
msgid "Gateway:"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:57
msgid "Optional"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:66
msgid "User name:"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:86
msgid "NT Domain:"
msgstr ""

#: src/vpn-editor/VpnEditor.qml:25
#, qt-format
msgid "Editing: %1"
msgstr ""

#: src/vpn-editor/VpnList.qml:26
#, fuzzy
#| msgid "VPN connection %1"
msgid "VPN configurations"
msgstr "Connection VPN %1"

#: src/vpn-editor/VpnList.qml:40
msgid "OpenVPN"
msgstr ""

#: src/vpn-editor/VpnList.qml:45
msgid "PPTP"
msgstr ""

#: src/vpn-editor/VpnList.qml:86
msgid "Delete configuration"
msgstr ""

#: src/vpn-editor/VpnList.qml:100
#, fuzzy
#| msgid "VPN connection %1"
msgid "No VPN connections"
msgstr "Connection VPN %1"
