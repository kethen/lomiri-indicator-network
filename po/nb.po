# Norwegian Bokmal translation for lomiri-indicator-network
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-indicator-network package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-network\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 19:46+0000\n"
"PO-Revision-Date: 2023-03-02 15:36+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/lomiri/"
"lomiri-indicator-network/nb_NO/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16.2-dev\n"
"X-Launchpad-Export-Date: 2016-10-21 07:05+0000\n"

#: doc/qt/qml/examples/example_networking_status.qml:46
#, fuzzy
msgid "Networking Status"
msgstr "Nettverksstatus"

#: src/agent/SecretRequest.cpp:61
#, qt-format
msgid "Connect to “%1”"
msgstr "Koble til «%1»"

#: src/agent/SecretRequest.cpp:66
msgid "WPA"
msgstr "WPA"

#: src/agent/SecretRequest.cpp:68
msgid "WEP"
msgstr "WEP"

#: src/agent/SecretRequest.cpp:74
msgid "Connect"
msgstr "Koble til"

#: src/agent/SecretRequest.cpp:75 src/vpn-editor/DialogFile/DialogFile.qml:156
msgid "Cancel"
msgstr "Avbryt"

#: src/indicator/factory.cpp:186
msgid "Wi-Fi"
msgstr "Trådløst nettverk"

#: src/indicator/factory.cpp:197
msgid "Flight Mode"
msgstr "Flymodus"

#: src/indicator/factory.cpp:207
msgid "Cellular data"
msgstr "Mobildata"

#: src/indicator/factory.cpp:220
msgid "Hotspot"
msgstr "Delt trådløst"

#: src/indicator/menuitems/wifi-link-item.cpp:109
msgid "Other network…"
msgstr "Annet nettverk …"

#: src/indicator/menuitems/wwan-link-item.cpp:90
msgid "No SIM"
msgstr "Ingen SIM-kort"

#: src/indicator/menuitems/wwan-link-item.cpp:97
msgid "SIM Error"
msgstr "Feil med SIM-kort"

#: src/indicator/menuitems/wwan-link-item.cpp:105
msgid "SIM Locked"
msgstr "SIM-kortet er låst"

#: src/indicator/menuitems/wwan-link-item.cpp:118
msgid "Unregistered"
msgstr "Ikke registrert"

#: src/indicator/menuitems/wwan-link-item.cpp:123
msgid "Unknown"
msgstr "Ukjent"

#: src/indicator/menuitems/wwan-link-item.cpp:128
msgid "Denied"
msgstr "Nektet"

#: src/indicator/menuitems/wwan-link-item.cpp:133
msgid "Searching"
msgstr "Søker"

#: src/indicator/menuitems/wwan-link-item.cpp:145
msgid "No Signal"
msgstr "Intet signal"

#: src/indicator/menuitems/wwan-link-item.cpp:157
#: src/indicator/menuitems/wwan-link-item.cpp:164
msgid "Offline"
msgstr "Frakoblet"

#: src/indicator/nmofono/vpn/vpn-manager.cpp:87
#, qt-format
msgid "VPN connection %1"
msgstr "VPN-tilkobling %1"

#. TRANSLATORS: this is the indicator title shown on the top header of the indicator area
#: src/indicator/root-state.cpp:314
msgid "Network"
msgstr "Nettverk"

#: src/indicator/sections/vpn-section.cpp:147
msgid "VPN settings…"
msgstr "VPN-innstillinger …"

#: src/indicator/sections/wifi-section.cpp:58
msgid "Wi-Fi settings…"
msgstr "Innstillinger for trådløst nettverk …"

#: src/indicator/sections/wwan-section.cpp:102
msgid "Cellular settings…"
msgstr "Mobilnett-innstillinger …"

#: src/indicator/sim-unlock-dialog.cpp:144
msgid "Sorry, incorrect %{1} PIN."
msgstr "Feil %{1}-PIN."

#: src/indicator/sim-unlock-dialog.cpp:149
#: src/indicator/sim-unlock-dialog.cpp:178
msgid "This will be your last attempt."
msgstr "Dette er ditt siste forsøk."

#: src/indicator/sim-unlock-dialog.cpp:151
msgid ""
"If %{1} PIN is entered incorrectly you will require your PUK code to unlock."
msgstr ""
"Hvis du taster feil kode for %{1} én gang til, må du bruke PUK-kode for å "
"låse opp SIM-kortet."

#: src/indicator/sim-unlock-dialog.cpp:161
msgid "Sorry, your %{1} is now blocked."
msgstr "Feil. Din %{1} er nå blokkert."

#: src/indicator/sim-unlock-dialog.cpp:166
msgid "Please enter your PUK code to unblock SIM card."
msgstr "Tast PUK-kode for å fjerne blokkering på SIM-kort."

#: src/indicator/sim-unlock-dialog.cpp:168
msgid "You may need to contact your network provider for PUK code."
msgstr "PUK-koden får du ved å kontakte telefonoperatøren din."

#: src/indicator/sim-unlock-dialog.cpp:176
#: src/indicator/sim-unlock-dialog.cpp:190
msgid "Sorry, incorrect PUK."
msgstr "Feil PUK."

#: src/indicator/sim-unlock-dialog.cpp:180
msgid ""
"If PUK code is entered incorrectly, your SIM card will be blocked and needs "
"replacement."
msgstr ""
"Hvis du taster feil PUK-kode én gang til, blir SIM-kortet blokkert "
"permanent. Det må i så fall byttes ut."

#: src/indicator/sim-unlock-dialog.cpp:182
msgid "Please contact your network provider."
msgstr "Kontakt telefonnettoperatøren din."

#: src/indicator/sim-unlock-dialog.cpp:192
msgid "Your SIM card is now permanently blocked and needs replacement."
msgstr "SIM-kortet ditt er nå blokkert permanent, og må byttes ut."

#: src/indicator/sim-unlock-dialog.cpp:194
msgid "Please contact your service provider."
msgstr "Kontakt telefonnettoperatøren."

#: src/indicator/sim-unlock-dialog.cpp:218
msgid "Sorry, incorrect PIN"
msgstr "Feil PIN"

#: src/indicator/sim-unlock-dialog.cpp:230
msgid "Sorry, incorrect PUK"
msgstr "Feil PUK"

#: src/indicator/sim-unlock-dialog.cpp:292
msgid "Enter %{1} PIN"
msgstr "Tast PIN for %{1}"

#: src/indicator/sim-unlock-dialog.cpp:300
msgid "Enter PUK code"
msgstr "Tast PUK-kode"

#: src/indicator/sim-unlock-dialog.cpp:304
msgid "Enter PUK code for %{1}"
msgstr "Tast PUK-kode for %{1}"

#: src/indicator/sim-unlock-dialog.cpp:315
#, c-format
msgid "1 attempt remaining"
msgid_plural "%d attempts remaining"
msgstr[0] "1 forsøk gjenstår"
msgstr[1] "%d forsøk gjenstår"

#: src/indicator/sim-unlock-dialog.cpp:326
msgid "Enter new %{1} PIN"
msgstr "Tast ny PIN-kode for %{1}"

#: src/indicator/sim-unlock-dialog.cpp:334
msgid "Confirm new %{1} PIN"
msgstr "Bekreft ny PIN-kode for %{1}"

#: src/indicator/sim-unlock-dialog.cpp:373
msgid "PIN codes did not match."
msgstr "PIN-kodene samsvarer ikke."

#: src/indicator/vpn-status-notifier.cpp:48
#, qt-format
msgid "The VPN connection '%1' failed."
msgstr "VPN-tilkoblinga «%1» mislyktes."

#: src/indicator/vpn-status-notifier.cpp:50
#, qt-format
msgid ""
"The VPN connection '%1' failed because the network connection was "
"interrupted."
msgstr "VPN-tilkoblinga «%1» mislyktes fordi nettverkstilkoblinga ble brutt."

#: src/indicator/vpn-status-notifier.cpp:51
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service stopped unexpectedly."
msgstr "VPN-tilkoblinga «%1» mislyktes fordi VPN-tjenesten stopped uventet."

#: src/indicator/vpn-status-notifier.cpp:52
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service returned invalid "
"configuration."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes fordi VPN-tjenesten har ugyldig oppsett."

#: src/indicator/vpn-status-notifier.cpp:53
#, qt-format
msgid ""
"The VPN connection '%1' failed because the connection attempt timed out."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes fordi tilkoblingsforsøket gikk ut på tid."

#: src/indicator/vpn-status-notifier.cpp:54
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service did not start in time."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes fordi VPN-tjenesten ikke startet i tide."

#: src/indicator/vpn-status-notifier.cpp:55
#, qt-format
msgid "The VPN connection '%1' failed because the VPN service failed to start."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes fordi VPN-tjenesten ikke klarte å starte."

#: src/indicator/vpn-status-notifier.cpp:56
#, qt-format
msgid "The VPN connection '%1' failed because there were no valid VPN secrets."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes fordi det ikke finnes noen hemmelige VPN-"
"nøkler."

#: src/indicator/vpn-status-notifier.cpp:57
#, qt-format
msgid "The VPN connection '%1' failed because of invalid VPN secrets."
msgstr ""
"VPN-tilkoblinga «%1» mislyktes på grunn av ugyldige hemmelige VPN-nøkler."

#: src/indicator/vpn-status-notifier.cpp:68
msgid "VPN Connection Failed"
msgstr "VPN-tilkobling mislyktes"

#: src/vpn-editor/DialogFile/DialogFile.qml:164
msgid "Accept"
msgstr "Godta"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:113 src/vpn-editor/Pptp/Advanced.qml:24
#: src/vpn-editor/Pptp/Editor.qml:92
msgid "Advanced"
msgstr "Avansert"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:44
#: src/vpn-editor/Pptp/Advanced.qml:44
#, fuzzy
msgid "Only use connection for VPN resources"
msgstr "Kun bruk tilkoblingen for VPN-ressurser"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:48
msgid "Use custom gateway port:"
msgstr "Bruk egendefinert portner-port:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:64
#, fuzzy
msgid "Use renegotiation interval:"
msgstr "Bruk regenereringsintervall:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:85
msgid "Use LZO data compression"
msgstr "Bruk LZO-datasammenpakking"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:94
#, fuzzy
#| msgid "VPN connection %1"
msgid "Use a TCP connection"
msgstr "VPN-tilkobling %1"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:98
msgid "Use custom virtual device type:"
msgstr "Bruk egendefinert virtuell enhetstype:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:105
msgid "TUN"
msgstr "TUN"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:106
msgid "TAP"
msgstr "TAP"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:117
msgid "(automatic)"
msgstr "(automatisk)"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:121
msgid "and name:"
msgstr "og navn:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:125
msgid "Use custom tunnel MTU:"
msgstr "Bruk egendefinert tunnel-MTU:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:142
msgid "Use custom UDP fragment size:"
msgstr "Bruk egendefinert UDP-fragmentstørrelse:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:164
#, fuzzy
msgid "Restrict tunnel TCP MSS"
msgstr "Begrens tunnel-TCP MSS"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:173
msgid "Randomize remote hosts"
msgstr "Randomiser fjernverter"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:28
#: src/vpn-editor/Openvpn/Editor.qml:132
msgid "Proxies"
msgstr "Mellomtjenere"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:43
msgid "Proxy type:"
msgstr "Mellomtjenertype:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:45
msgid "Not required"
msgstr "Ikke påkrevd"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:46
msgid "HTTP"
msgstr "HTTP"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:47
msgid "SOCKS"
msgstr "SOCKS"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:54
msgid "Server address:"
msgstr "Tjeneradresse:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:65
msgid "Port:"
msgstr "Port:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:77
msgid "Retry indefinitely:"
msgstr "Prøv igjen uendelig:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:87
msgid "Proxy username:"
msgstr "Mellomtjener-brukernavn:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:98
msgid "Proxy password:"
msgstr "Mellomtjener-passord:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:119 src/vpn-editor/Pptp/Advanced.qml:97
msgid "Security"
msgstr "Sikkerhet"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:39
msgid "Cipher:"
msgstr "Krypteringsalgoritme:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:41
#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:83
msgid "Default"
msgstr "Forvalg"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:42
msgid "DES-CBC"
msgstr "DES-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:43
msgid "RC2-CBC"
msgstr "RC2-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:44
msgid "DES-EDE-CBC"
msgstr "DES-EDE-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:45
msgid "DES-EDE3-CBC"
msgstr "DES-EDE3-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:46
msgid "DESX-CBC"
msgstr "DESX-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:47
msgid "RC2-40-CBC"
msgstr "RC2-40-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:48
msgid "CAST5-CBC"
msgstr "CAST5-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:49
msgid "AES-128-CBC"
msgstr "AES-128-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:50
msgid "AES-192-CBC"
msgstr "AES-192-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:51
msgid "AES-256-CBC"
msgstr "AES-256-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:52
msgid "CAMELLIA-128-CBC"
msgstr "CAMELLIA-128-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:53
msgid "CAMELLIA-192-CBC"
msgstr "CAMELLIA-192-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:54
msgid "CAMELLIA-256-CBC"
msgstr "CAMELLIA-256-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:55
msgid "SEED-CBC"
msgstr "SEED-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:56
msgid "AES-128-CBC-HMAC-SHA1"
msgstr "AES-128-CBC-HMAC-SHA1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:57
msgid "AES-256-CBC-HMAC-SHA1"
msgstr "AES-256-CBC-HMAC-SHA1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:64
#, fuzzy
msgid "Use cipher key size:"
msgstr "Brukt krypteringsalgoritme-nøkkelstørrelse:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:81
msgid "HMAC authentication:"
msgstr "HMAC-identitetsbekreftelse:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:84
#: src/vpn-editor/Openvpn/AdvancedTls.qml:86
#: src/vpn-editor/Openvpn/StaticKey.qml:37
msgid "None"
msgstr "Ingen"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:85
msgid "RSA MD-4"
msgstr "RSA MD-4"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:86
msgid "MD-5"
msgstr "MD-5"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:87
msgid "SHA-1"
msgstr "SHA-1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:88
#, fuzzy
msgid "SHA-224"
msgstr "SHA-244"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:89
msgid "SHA-256"
msgstr "SHA-256"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:90
msgid "SHA-384"
msgstr "SHA-384"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:91
msgid "SHA-512"
msgstr "SHA-512"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:92
msgid "RIPEMD-160"
msgstr "RIPEMD-160"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:25
msgid "TLS authentication:"
msgstr "TLS-identitetsbekreftelse:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:46
#, fuzzy
msgid "Subject match:"
msgstr "Emneoverensstemmelse:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:50
msgid "Verify peer certificate:"
msgstr "Bekreft likemannssertifikat:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:57
msgid "Peer certificate TLS type:"
msgstr "TLS-type for likemannssertifikat:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:59
msgid "Server"
msgstr "Tjener"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:60
msgid "Client"
msgstr "Klient"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:68
msgid "Use additional TLS authentication:"
msgstr "Bruk ytterligere TLS-identitetsbekreftelse:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:75
msgid "Key file:"
msgstr "Nøkkelfil:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:84
#: src/vpn-editor/Openvpn/StaticKey.qml:35
msgid "Key direction:"
msgstr "Nøkkelretning:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:87
#: src/vpn-editor/Openvpn/StaticKey.qml:38
msgid "0"
msgstr "0"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:88
#: src/vpn-editor/Openvpn/StaticKey.qml:39
msgid "1"
msgstr "1"

#: src/vpn-editor/Openvpn/Editor.qml:65 src/vpn-editor/Pptp/Editor.qml:35
msgid "General"
msgstr "Generelt"

#: src/vpn-editor/Openvpn/Editor.qml:74 src/vpn-editor/Pptp/Editor.qml:44
msgid "ID:"
msgstr "ID:"

#: src/vpn-editor/Openvpn/Editor.qml:77
msgid "Authentication"
msgstr "Identitetsbekreftelse"

#: src/vpn-editor/Openvpn/Editor.qml:86
msgid "Remote:"
msgstr "Annensteds hen:"

#: src/vpn-editor/Openvpn/Editor.qml:91
msgid "Certificates (TLS)"
msgstr "Sertifikat (TLS)"

#: src/vpn-editor/Openvpn/Editor.qml:92
msgid "Password"
msgstr "Passord"

#: src/vpn-editor/Openvpn/Editor.qml:93
msgid "Password with certificates (TLS)"
msgstr "Passord med sertifikater (TLS)"

#: src/vpn-editor/Openvpn/Editor.qml:94
msgid "Static key"
msgstr "Statisk nøkkel"

#: src/vpn-editor/Openvpn/Editor.qml:98
msgid "Type:"
msgstr "Type:"

#: src/vpn-editor/Openvpn/Editor.qml:125
msgid "TLS"
msgstr "TLS"

#: src/vpn-editor/Openvpn/Password.qml:32
#: src/vpn-editor/Openvpn/PasswordTls.qml:32
msgid "Username:"
msgstr "Brukernavn:"

#: src/vpn-editor/Openvpn/Password.qml:42
#: src/vpn-editor/Openvpn/PasswordTls.qml:42 src/vpn-editor/Pptp/Editor.qml:76
msgid "Password:"
msgstr "Passord:"

#: src/vpn-editor/Openvpn/Password.qml:51
#: src/vpn-editor/Openvpn/PasswordTls.qml:60 src/vpn-editor/Openvpn/Tls.qml:40
msgid "CA certificate:"
msgstr "CA-sertifikat:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:51 src/vpn-editor/Openvpn/Tls.qml:31
msgid "User certificate:"
msgstr "Brukersertifikat:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:69 src/vpn-editor/Openvpn/Tls.qml:49
msgid "Private key:"
msgstr "Privat nøkkel:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:79 src/vpn-editor/Openvpn/Tls.qml:58
#, fuzzy
msgid "Key password:"
msgstr "Nøkkel-passord:"

#: src/vpn-editor/Openvpn/StaticKey.qml:31
msgid "Static key:"
msgstr "Statisk nøkkel:"

#: src/vpn-editor/Openvpn/StaticKey.qml:52
msgid "Remote IP:"
msgstr "Fjern-IP:"

#: src/vpn-editor/Openvpn/StaticKey.qml:62
msgid "Local IP:"
msgstr "Lokal IP:"

#: src/vpn-editor/Pptp/Advanced.qml:47
msgid "Authentication methods"
msgstr "Identitetbekreftelsesmetoder:"

#: src/vpn-editor/Pptp/Advanced.qml:55
msgid "PAP"
msgstr "PAP"

#: src/vpn-editor/Pptp/Advanced.qml:65
msgid "CHAP"
msgstr "CHAP"

#: src/vpn-editor/Pptp/Advanced.qml:75
msgid "MSCHAP"
msgstr "MSCHAP"

#: src/vpn-editor/Pptp/Advanced.qml:84
msgid "MSCHAPv2"
msgstr "MSCHAPv2"

#: src/vpn-editor/Pptp/Advanced.qml:93
msgid "EAP"
msgstr "EAP"

#: src/vpn-editor/Pptp/Advanced.qml:105
msgid "Use Point-to-Point encryption"
msgstr "Bruk punkt-til-punkt -kryptering"

#: src/vpn-editor/Pptp/Advanced.qml:110
msgid "All available (default)"
msgstr "Alle tilgjengelige (forvalg)"

#: src/vpn-editor/Pptp/Advanced.qml:111
msgid "128-bit (most secure)"
msgstr "128-bit (mest sikker)"

#: src/vpn-editor/Pptp/Advanced.qml:112
msgid "40-bit (less secure)"
msgstr "40-bit (mindre sikkert)"

#: src/vpn-editor/Pptp/Advanced.qml:125
#, fuzzy
msgid "Allow stateful encryption"
msgstr "Tillat tilstandsbasert kryptering"

#: src/vpn-editor/Pptp/Advanced.qml:128
msgid "Compression"
msgstr "Sammenpakking"

#: src/vpn-editor/Pptp/Advanced.qml:136
msgid "Allow BSD data compression"
msgstr "Tillat BSD-datasammenpakking"

#: src/vpn-editor/Pptp/Advanced.qml:145
msgid "Allow Deflate data compression"
msgstr "Tillat Deflate-sammenpakking"

#: src/vpn-editor/Pptp/Advanced.qml:154
msgid "Use TCP header compression"
msgstr "Bruk TCP-hode-sammenpakking"

#: src/vpn-editor/Pptp/Advanced.qml:157
#, fuzzy
msgid "Echo"
msgstr "Ekko"

#: src/vpn-editor/Pptp/Advanced.qml:165
#, fuzzy
msgid "Send PPP echo packets"
msgstr "Send PPP-ekko-pakker"

#: src/vpn-editor/Pptp/Editor.qml:54
msgid "Gateway:"
msgstr "Portner:"

#: src/vpn-editor/Pptp/Editor.qml:57
msgid "Optional"
msgstr "Valgfritt"

#: src/vpn-editor/Pptp/Editor.qml:66
#, fuzzy
msgid "User name:"
msgstr "Brukernavn:"

#: src/vpn-editor/Pptp/Editor.qml:86
#, fuzzy
msgid "NT Domain:"
msgstr "NT-domene:"

#: src/vpn-editor/VpnEditor.qml:25
#, fuzzy, qt-format
msgid "Editing: %1"
msgstr "Redigering: %1"

#: src/vpn-editor/VpnList.qml:26
msgid "VPN configurations"
msgstr "VPN-oppsett"

#: src/vpn-editor/VpnList.qml:40
msgid "OpenVPN"
msgstr "OpenVPN"

#: src/vpn-editor/VpnList.qml:45
msgid "PPTP"
msgstr "PPTP"

#: src/vpn-editor/VpnList.qml:86
msgid "Delete configuration"
msgstr "Slett oppsett"

#: src/vpn-editor/VpnList.qml:100
msgid "No VPN connections"
msgstr "Ingen VPN-tilkoblinger"
