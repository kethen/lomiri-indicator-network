dbus-send --session --print-reply           \
    --dest=com.lomiri.connectivity1         \
    /com/lomiri/connectivity1/Private       \
    org.freedesktop.DBus.Properties.Set     \
    string:com.lomiri.connectivity1.Private \
    string:MobileDataEnabled                \
    variant:boolean:false
sh get-mobile-data-enabled.sh
